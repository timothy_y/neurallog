﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using NeuralNet;
using NeuralNet.Configuration;
using NeuralNetTset;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace NeuroLog.UI
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class TrainPage : Page
	{
		/// <summary>
		/// 
		/// </summary>
		public TrainPage()
		{

			InitializeComponent();

			Init();

			var trainList = new List<string>
			{
				"Not",
				"Or",
				"And",
				"Nand",
				"Xor",
				"MNIST"
			};
			trainList.ToList().ForEach(item => { TrainCombo.Items.Add(item); });
			
			

			Start.Click += async (sender, args) =>
			{
				ConsoleLog.Clear();
				PlotViewer.ClearPlot();
				var trainDialog = new ContentDialog
				{
					Title = "Train option",
					Content = "Start train with new network?",
					CloseButtonText = "Create new",
					PrimaryButtonText = "Use old",
					DefaultButton = ContentDialogButton.Primary
				};
				trainDialog.PrimaryButtonClick += (dialog, eventArgs) => Mnist.Train(int.TryParse(SizeBox.Text, out var size) ? size : 20, double.TryParse(LearnRateBox.Text, out var leartRate) ? leartRate : 0.5, true);

				trainDialog.CloseButtonClick += (dialog, eventArgs) => Mnist.Train(int.TryParse(SizeBox.Text, out var size) ? size : 20, double.TryParse(LearnRateBox.Text, out var leartRate) ? leartRate : 0.5, false);

				await trainDialog.ShowAsync();
			};

			BackButton.Click += (sender, args) => Frame.Navigate(typeof(MainPage));

			Stop.Click += (sender, args) => TempData.IsStop = true;
		}

		private void Init()
		{
			TempData.CurrentPage = this;
			TempData.Console = Console;
			TempData.Plot = Plot;
			TempData.PlotScroll = PlotScroll;
			Plot.InitPlot();
		}
	}
}
