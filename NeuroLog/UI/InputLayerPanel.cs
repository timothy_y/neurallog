﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using NeuralNet;

namespace NeuroLog.UI
{
	/// <summary>
	/// 
	/// </summary>
	public class InputLayerPanel
	{
		/// <summary>
		/// 
		/// </summary>
		public int NeuronCount { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public InputLayerPanel()
		{
			NeuronCount = 0;
		}
	}
	/// <summary>
	/// 
	/// </summary>
	public class InputLayerViewModel
	{
		/// <summary>
		/// 
		/// </summary>
		public InputLayerPanel DefaultPanel => new InputLayerPanel();
	}
}

