﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Imaging;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Microsoft.WindowsAzure.MobileServices;
using NeuralNet;
using NeuralNet.Model;
using NeuralNetTset;
using Newtonsoft.Json;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace NeuroLog.UI
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class TestingPage : Page
	{
		/// <summary>
		/// 
		/// </summary>
		private StorageFile _imageFile { get; set; }

		/// <summary>
		/// 
		/// </summary>
		private Network _network { get; set; } 

		private List<double> pixels { get; set; } = new List<double>();

		/// <summary>
		/// 
		/// </summary>
		public TestingPage()
		{
			this.InitializeComponent();

			TempData.CurrentPage = this;
			TempData.Console = Console;

			BackButton.Click += (sender, args) => Frame.Navigate(typeof(MainPage));
		}

		private async Task Init()
		{
			_network = await Network.RecoverNetwork("mnist_test");
		}

		private async void PickButton_Click(object sender, RoutedEventArgs e)
		{
			await Init();

			var picker = new Windows.Storage.Pickers.FileOpenPicker
			{
				ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail,
				SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary
			};

			picker.FileTypeFilter.Add(".jpg");
			picker.FileTypeFilter.Add(".jpeg");
			picker.FileTypeFilter.Add(".png");

			_imageFile = await picker.PickSingleFileAsync();
			

			if (_imageFile != null)
			{
				var fileStream = await _imageFile.OpenAsync(Windows.Storage.FileAccessMode.Read);

				var bitmapImage = new BitmapImage();

				await bitmapImage.SetSourceAsync(fileStream);

				Image.Source = bitmapImage;

				StorageFile file = _imageFile;
				using (var stream = await file.OpenAsync(FileAccessMode.Read))
				{
					BitmapDecoder decoder = await BitmapDecoder.CreateAsync(stream);
					WriteableBitmap bmp = new WriteableBitmap((int) decoder.PixelWidth, (int) decoder.PixelHeight);
					bmp.SetSource(stream);

					for (int i = 0; i < 28; ++i)
					{
						for (int j = 0; j < 28; ++j)
						{
							pixels.Add(bmp.GetPixel(j, i).R == 0 ? 0d : 1d);
						}
					}
				}

				
			}
			else
			{
				
			}
		}

		private void GuessButton_Click(object sender, RoutedEventArgs e)
		{
			var guessArr = _network.Guess(pixels);
			var anwser = Mnist.IndexOfMaxValue(guessArr.ToArray());
			pixels.Clear();
			ConsoleLog.WriteLine($"I think the number is {anwser}");
		}
	}
}
