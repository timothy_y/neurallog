﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using NeuralNet;

namespace NeuroLog.UI
{
	/// <summary>
	/// 
	/// </summary>
	public class OutputLayerPanel
	{
		/// <summary>
		/// 
		/// </summary>
		public int NeuronCount { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public ActivatorType ActivatorType { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public List<string> ActivatorsList { get; set; } = Enum.GetNames(typeof(ActivatorType)).ToList();

		/// <summary>
		/// 
		/// </summary>
		public OutputLayerPanel()
		{
			NeuronCount = 0;

			ActivatorType = ActivatorType.SigmoidActivator;
		}
	}
	/// <summary>
	/// 
	/// </summary>
	public class OutputLayerViewModel
	{
		/// <summary>
		/// 
		/// </summary>
		public OutputLayerPanel DefaultPanel => new OutputLayerPanel();
	}
}

