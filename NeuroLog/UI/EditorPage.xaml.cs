﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace NeuroLog.UI
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class EditorPage : Page
	{

		/// <summary>
		/// 
		/// </summary>
		public EditorPage()
		{
			this.InitializeComponent();

			InputLayerList.Items?.Add(new InputLayerPanel());
			HiddenLayerList.Items?.Add(new HiddenLayerPanel());
			OutputLayerList.Items?.Add(new OutputLayerPanel());

			AddLayerButton.Click += (sender, args) => HiddenLayerList.Items?.Add(new HiddenLayerPanel());

			RemoveLayerButton.Click += (sender, args) =>
			{
				if (HiddenLayerList.Items?.Count > 1)
				{
					HiddenLayerList.Items?.Remove(HiddenLayerList.Items?.Last());
				}
				
			};
		}
	}
}
