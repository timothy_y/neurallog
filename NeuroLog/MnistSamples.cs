﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuralNetTset;

namespace NeuralNet
{
	class MnistSamples
	{
		/// <summary>
		/// 
		/// </summary>
		public List<Sample> Training { get; } = new List<Sample>();

		/// <summary>
		/// 
		/// </summary>
		public List<Sample> Testing { get; } = new List<Sample>();

		public async Task GerMnistSamples()
		{
			var imageFile =
				await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(@"t10k-images.idx3-ubyte");

			var imgBuffer = await Windows.Storage.FileIO.ReadBufferAsync(imageFile);
			var imagesReader = Windows.Storage.Streams.DataReader.FromBuffer(imgBuffer);

			var labelFile =
				await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(@"t10k-labels.idx1-ubyte");

			var lblBuffer = await Windows.Storage.FileIO.ReadBufferAsync(labelFile);
			var labelsReader = Windows.Storage.Streams.DataReader.FromBuffer(lblBuffer);

			imagesReader.ReadInt32();
			imagesReader.ReadInt32();
			imagesReader.ReadInt32();
			imagesReader.ReadInt32();

			labelsReader.ReadInt32();
			labelsReader.ReadInt32();

			for (int di = 1; di < 10000; ++di)
			{
				var pixels = new double[784];
				for (int i = 0; i < 784; ++i)
				{
					byte b = imagesReader.ReadByte();
					pixels[i] = b == 0 ? 0 : 1;
				}

				
				var anwser = Convert.ToInt16(labelsReader.ReadByte());
				if (anwser > 9)
				{
					continue;
				}
				var target = Enumerable.Repeat(0d, 10).ToArray();
				target[anwser] = 1;

				Training.Add(new Sample(pixels, target));
			}
			labelsReader.Dispose();
			imagesReader.Dispose();
		}
	}
}
