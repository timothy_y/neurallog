﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Microsoft.Toolkit.Uwp.Helpers;
using Microsoft.WindowsAzure.MobileServices;
using Microsoft.WindowsAzure.MobileServices.Sync;
using NeuralNet;

namespace NeuralNetTset
{
	/// <summary>
	/// 
	/// </summary>
	public static class TempData
	{
		/// <summary>
		/// 
		/// </summary>
		public static TextBox Console { get; internal set; }

		/// <summary>
		///
		/// </summary>
		public static Canvas Plot { get; internal set; }

		/// <summary>
		/// 
		/// </summary>
		public static Page CurrentPage { get; internal set; }

		/// <summary>
		/// 
		/// </summary>
		public static LocalObjectStorageHelper Memory { get; } = new LocalObjectStorageHelper();

		/// <summary>
		/// 
		/// </summary>
		internal static Network LastNetwork { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public static bool IsStop { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public static ScrollViewer PlotScroll { get; set; }
	}
}
