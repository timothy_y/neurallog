﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuralNet.Configuration;
using NeuralNetTset;

namespace NeuralNet
{
	class Mnist
	{
		public static async void Train(int trainingCount, double learnRate, bool isOld)
		{
			ConsoleLog.WriteLine("Starting MNIST training. Loading samples");
			var mnistSamples = new MnistSamples();
			ConsoleLog.WriteLine("Done");

			ConsoleLog.Write(isOld ? "Loading saved network" : "Creating network");

			Network net;

			if (isOld)
			{
				net = await Network.RecoverNetwork("mnist_test");
			}
			else
			{
				var layerConfig = new LayerConfiguration();

				layerConfig
					.AddInputLayer(784)
					.AddHiddenLayer(500, ActivatorType.SigmoidActivator)
					.AddHiddenLayer(100, ActivatorType.SigmoidActivator)
					.AddOutputLayer(10, ActivatorType.SigmoidActivator);

				var config = new NetworkConfiguration(layerConfig)
				{
					Momentum = 0,
					LearningRate = learnRate
				};

				net = new Network(config);
			}

			ConsoleLog.WriteLine("Done");

			await mnistSamples.GerMnistSamples();
			var training = mnistSamples.Training.Take(trainingCount).ToArray();

			new Trainer(net, training, CheckCorrect).StartTraining();
		}

		static bool CheckCorrect(double[] target, double[] output)
		{
			return IndexOfMaxValue(target) == IndexOfMaxValue(output);
		}

		public static int IndexOfMaxValue(double[] numbers)
		{
			var index = 0;
			return numbers
				.Select(number => new {index = index++, number})
				.OrderBy(t => t.number)
				.Last().index;
		}
	}

}
