﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;
using NeuralNetTset;

namespace NeuroLog
{
	public static class PlotViewer
	{
		/// <summary>
		/// 
		/// </summary>
		private static readonly Canvas _plotCanvas = TempData.Plot;

		/// <summary>
		/// 
		/// </summary>
		private static List<Point> _plotPoints { get; } = new List<Point>();

		/// <summary>
		/// 
		/// </summary>
		private static Polyline _polyline;

		public static void AddValue(Point point)
		{
			_plotPoints.Add(point);
		}

		public static void UpdatePlot()
		{
			_plotCanvas.InitPlot();
			_polyline = new Polyline
			{
				Stroke = new SolidColorBrush(Colors.Teal),
				Fill = new SolidColorBrush(Color.FromArgb(60, 0, 100, 90)),
				StrokeThickness = 5,
				Margin = new Thickness(0, 0, 0, 0)
			};

			_polyline.Points.Add(FormatPoint(new Point(0,0)));
			_plotPoints.ForEach(plotPoint =>
			{
				_polyline.Points.Add(FormatPoint(plotPoint));
				_plotCanvas.Children.Add(new Line
				{
					X1 = FormatPoint(plotPoint).X,
					X2 = FormatPoint(plotPoint).X,
					Y1 = 0,
					Y2 = _plotCanvas.Height,
					Stroke = new SolidColorBrush(Color.FromArgb(40, 255, 255, 255)),
					StrokeThickness = 2
				});
			});
			_polyline.Points.Add(FormatPoint(new Point(_plotPoints.Count, 0)));
			_plotCanvas.Children.Add(_polyline);
			TempData.PlotScroll.ChangeView(double.MaxValue, 0.0f, 1.0f);
		}

		/// <summary>
		/// 
		/// </summary>
		public static void ClearPlot()
		{
			//TempData.ButtomLegend.Children.Clear();
			_plotPoints.Clear();
			_plotCanvas.InitPlot();
		}
	 

		private static Point FormatPoint(Point point) => new Point
		{
			X =  _plotCanvas.Width * point.X / _plotPoints.Count,
			Y = _plotCanvas.Height - point.Y * _plotCanvas.Height / 100
		};

		public static void InitPlot(this Canvas canvas)
		{
			canvas.Children.Clear();
			_plotCanvas.Width = _plotPoints.Count < 10 ? 1800 : 1800 + (_plotPoints.Count - 10) * 50;
			for (int i = 0; i < 10; i++)
			{
				canvas.Children.Add(new Line
				{
					X1 = 0,
					X2 = canvas.Width,
					Y1 = i * (canvas.Height / 10) + (canvas.Height / 10),
					Y2 = i * (canvas.Height / 10) + (canvas.Height / 10),
					Stroke = new SolidColorBrush(Color.FromArgb(40, 255, 255, 255)),
					StrokeThickness = 1
				});
				canvas.Children.Add(new TextBlock
				{
					Margin = new Thickness(-50, i * (canvas.Height / 10), 0, 0),
					Text = String.Format("{0} %", (10 - i) * 10)
				});
			}
		}
	}
}
