﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuralNet.Activators;
using NeuralNet.Neurons;

namespace NeuralNet
{
	class HiddenLayer : Layer
	{

		/// <summary>
		/// 
		/// </summary>
		public override Neuron[] Neurons => _neurons;

		private readonly Neuron[] _neurons;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="activation"></param>
		/// <param name="trainInfo"></param>
		/// <param name="size"></param>
		//public HiddenLayer(IActivator activation, IWeightInitializer weightInitializer, int size)
		//{
		//	var neurons = new Neuron[size + 1];
		//	for (var i = 0; i < size; i++)
		//	{
		//		neurons[i] = new HiddenNeuron(activation, weightInitializer);
		//	}

		//	neurons[size] = new BiasNeuron();
		//	Neurons = neurons;
		//}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="neurons"></param>
		/// <param name="layerIndex"></param>
		public HiddenLayer(Neuron[] neurons, int layerIndex)
		{
			LayerIndex = layerIndex;
			_neurons = neurons;
		}

	

		
	}
}
