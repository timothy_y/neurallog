﻿namespace NeuralNet
{
    internal enum LayerType
    {
        Input,
        Hidden,
        Output
    }
}
