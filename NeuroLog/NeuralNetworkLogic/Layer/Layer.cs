﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuralNet.Neurons;

namespace NeuralNet
{
	abstract class Layer
	{
		/// <summary>
		/// 
		/// </summary>
		public abstract Neuron[] Neurons { get; }

		public int LayerIndex { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public void FeedForward()
		{
			foreach (var neuron in Neurons)
			{
				neuron.FeedForward();
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public void PropagateBack()
		{
			foreach (var neuron in Neurons)
			{
				neuron.PropagateBack();
			}
		}

		public LayerType LayerType { get; set; }
	}
}
