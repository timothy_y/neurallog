﻿using System;
using System.Collections.Generic;
using System.Linq;
using NeuralNet.Activators;
using NeuralNet.Model;
using NeuralNet.Neurons;

namespace NeuralNet
{
	/// <summary>
	/// 
	/// </summary>
	class OutputLayer : Layer
	{
		/// <summary>
		/// 
		/// </summary>
		private readonly OutputNeuron[] _neurons;

		//public OutputLayer(IActivator activation, IWeightInitializer trainInfo, int size)
		//{
		//	var neurons = new OutputNeuron[size];
		//	for (int i = 0; i < size; i++)
		//	{
		//		neurons[i] = new OutputNeuron(activation, trainInfo);
		//	}
		//	_neurons = neurons;
		//}

		/// <summary>
		/// 
		/// </summary>
		public override Neuron[] Neurons => _neurons;

		public double[] Values
		{
			get { return _neurons.Select(n => n.Value).ToArray(); }
		}

		public void SetTargetValues(double[] targetValues)
		{
			for (int i = 0; i < targetValues.Length; i++)
				_neurons[i].TargetValue = targetValues[i];
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="neurons"></param>
		/// <param name="layerIndex"></param>
		public OutputLayer(OutputNeuron[] neurons, int layerIndex)
		{
			LayerIndex = layerIndex;
			_neurons = neurons;
		}
	}
}
