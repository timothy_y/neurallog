﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuralNet.Neurons;

namespace NeuralNet
{
	/// <summary>
	/// 
	/// </summary>
	class InputLayer : Layer
	{
		private readonly InputNeuron[] _neurons; // inputNeurons + Bias

		/// <summary>
		/// 
		/// </summary>
		/// <param name="size"></param>
		//public InputLayer(int size)
		//{
		//	var neurons = new InputNeuron[size + 1];

		//	for (var i = 0; i < size; i++)
		//	{
		//		neurons[i] = new InputNeuron();
		//	}

		//	neurons[size] = new BiasNeuron();
		//	_neurons = neurons;
		//}

		/// <summary>
		/// 
		/// </summary>
		public override Neuron[] Neurons => _neurons;

		/// <summary>
		/// </summary>
		/// <param name="input"></param>
		public void SetInputValues(double[] input)
		{
			for (int i = 0; i < input.Length; i++)
			{
				_neurons[i].SetValue(input[i]);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="neurons"></param>
		public InputLayer(InputNeuron[] neurons)
		{
			LayerIndex = 0;
			_neurons = neurons;
		}
	}
}
