﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuralNet.Connections;
using NeuralNet.Model;

namespace NeuralNet
{
	class InputNeuron : Neuron
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="neuronIndex"></param>
		public InputNeuron(int neuronIndex) : base(neuronIndex)
		{
			
		}

		/// <summary>
		/// 
		/// </summary>
		public override void FeedForward()
		{
			// do nothing  
		}

		/// <summary>
		/// 
		/// </summary>
		public override void PropagateBack()
		{
			// do nothing
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public virtual void SetValue(double value)
		{
			Value = value;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="connections"></param>
		public override void SetBackwardConnections(List<Connection> connections)
		{
			throw new NotSupportedException();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		protected override double CalcValueDelta()
		{
			throw new Exception("Don't expect CalcValueDelta to be called on a Value neuron because PropagateBack does nothing");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		internal override NeuronModel ToNeuronModel()
		{
			return new NeuronModel
			{
				NeuronIndex = NeuronIndex,
				Activator = string.Empty,
				Weights = ForwardConnections
					.Select(q => q.Weight)
					.ToList()
			};
		}
	}
}
