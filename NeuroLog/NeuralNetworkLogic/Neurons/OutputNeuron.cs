﻿using System;
using System.Collections.Generic;
using NeuralNet.Activators;
using NeuralNet.Connections;
using NeuralNet.Model;
using NeuroLog.NeuralNetworkLogic.Neurons;

namespace NeuralNet.Neurons
{
	/// <summary>
	/// 
	/// </summary>
	class OutputNeuron : Neuron
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="neuronIndex"></param>
		/// <param name="activator"></param>
		/// <param name="weightInitializer"></param>
		public OutputNeuron(int neuronIndex, IActivator activator, IWeightInitializer weightInitializer, NeuronParam neuronParam = null) : base(neuronIndex, activator, weightInitializer, neuronParam)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		public double TargetValue { private get; set; }

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		protected override double CalcValueDelta()
		{
			return TargetValue - Value;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="connections"></param>
		public override void SetForwardConnections(List<Connection> connections)
		{
			throw new NotSupportedException();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		internal override NeuronModel ToNeuronModel()
		{
			return new NeuronModel
			{
				NeuronIndex = NeuronIndex,
				Activator = Activation.GetType().AssemblyQualifiedName,
				Weights = new List<double>()
			};
		}
	}
}