﻿namespace NeuroLog.NeuralNetworkLogic.Neurons
{
	public class NeuronParam
	{
		public double LearnRate { get; set; } = 0.5;

		public double Momentum { get; set; }
	}
}