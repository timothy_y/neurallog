﻿using System;
using System.Collections.Generic;
using NeuralNet.Activators;
using NeuralNet.Connections;

namespace NeuralNet.Neurons
{
	/// <summary>
	/// 
	/// </summary>
	class BiasNeuron : InputNeuron
	{
		/// <summary>
		/// 
		/// </summary>
		public BiasNeuron(int neuronIndex) : base(neuronIndex)
		{
			base.SetValue(1);
			Activation = new SigmoidActivation();
		}

		public override void SetBackwardConnections(List<Connection> connections)
		{
			
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="value"></param>
		public override void SetValue(double value)
		{
			throw new Exception("You can't set the value of a BiasNeuron");
		}

	}
}