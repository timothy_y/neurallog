﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NeuralNet.Activators;
using NeuralNet.Connections;
using NeuralNet.Model;
using NeuralNet.WeightInitializers;
using NeuroLog.NeuralNetworkLogic.Neurons;
using Newtonsoft.Json;

namespace NeuralNet
{
	abstract class Neuron
	{
		/// <summary>
		/// 
		/// </summary>
		internal IActivator Activation { get; set; } = new SigmoidActivation();

		/// <summary>
		/// 
		/// </summary>
		public IWeightInitializer WeightInitializer { get; set; } = new DefaultWeightInitializer();

		/// <summary>
		/// 
		/// </summary>
		public readonly int NeuronIndex;

		public readonly NeuronParam NeuronParam;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="activation"></param>
		/// <param name="trainInfo"></param>
		protected Neuron(int neuronIndex, IActivator activation, IWeightInitializer weightInitializer, NeuronParam neuronParam = null)
		{
			NeuronIndex = neuronIndex;
			this.Activation = activation;
			WeightInitializer = weightInitializer;
			NeuronParam = neuronParam ?? new NeuronParam(){LearnRate = 0.5};
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="neuronIndex"></param>
		protected Neuron(int neuronIndex)
		{
			NeuronIndex = neuronIndex;
			ForwardConnections = new List<Connection>();
			BackwardConnections = new List<Connection>();
		}

		/// <summary>
		/// 
		/// </summary>
		public List<Connection> BackwardConnections = new List<Connection>();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="connection"></param>
		public void AddBackwardConnections(Connection connection)
		{
			BackwardConnections.Add(connection);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="connections"></param>
		public virtual void SetBackwardConnections(List<Connection> connections)
		{
			BackwardConnections.AddRange(connections);
		}

		/// <summary>
		/// 
		/// </summary>
		public List<Connection> ForwardConnections = new List<Connection>();

		/// <summary>
		/// 
		/// </summary>
		/// <param name="connection"></param>
		public void AddForwardConnections(Connection connection)
		{
			ForwardConnections.Add(connection);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="connections"></param>
		public virtual void SetForwardConnections(List<Connection> connections)
		{
			ForwardConnections.AddRange(connections);
		}

		/// <summary>
		/// 
		/// </summary>
		public double Value { get; protected set; }

		/// <summary>
		/// 
		/// </summary>
		public double Error { get; protected set; }

		/// <summary>
		/// 
		/// </summary>
		public virtual void FeedForward()
		{
			double sum = 0;
			foreach (var conn in BackwardConnections)
			{
				sum += conn.WeightedValue;
			}

			Value = Activation.CalculateValue(sum);
		}

		protected abstract double CalcValueDelta();

		/// <summary>
		/// 
		/// </summary>
		public virtual void PropagateBack()
		{
			var valueDelta = CalcValueDelta();
			Error = valueDelta * Activation.CalculateDerivative(Value);

			foreach (var conn in BackwardConnections)
			{
				conn.PropagateBack(NeuronParam.LearnRate, Error);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		internal virtual NeuronModel ToNeuronModel()
		{
			return new NeuronModel
			{
				NeuronIndex = NeuronIndex,
				Activator = Activation.GetType().AssemblyQualifiedName,
				Weights = ForwardConnections
					.Select(q => q.Weight)
					.ToList()
			};
		}
	}
}
