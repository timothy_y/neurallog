﻿using System.Linq;
using NeuralNet.Activators;
using NeuroLog.NeuralNetworkLogic.Neurons;

namespace NeuralNet
{
	/// <summary>
	/// 
	/// </summary>
	class HiddenNeuron : Neuron
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="neuronIndex"></param>
		/// <param name="activator"></param>
		/// <param name="weightInitializer"></param>
		public HiddenNeuron(int neuronIndex, IActivator activator, IWeightInitializer weightInitializer, NeuronParam neuronParam = null) : base(neuronIndex, activator, weightInitializer, neuronParam)
		{

		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		protected override double CalcValueDelta()
		{
			return ForwardConnections.Sum(conn => conn.WeightedError);
		}
	}
}