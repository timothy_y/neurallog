﻿using System;

namespace NeuralNet.Activators
{
	/// <summary>
	/// 
	/// </summary>
	public class ReluActivator : IActivator
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public double CalculateDerivative(double input)
		{
			return input < 0 ? 0 : 1;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public double CalculateValue(double input)
		{
			return Math.Max(0, input);
		}

		/// <summary>
		/// 
		/// </summary>
		private static Random _rand = new Random();

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public double GetRandomWeight()
		{
			return (_rand.NextDouble() * 2) - 1;
		}

		public static IActivator ActivatorInstance { get; set; } = new ReluActivator();
	}
}
