﻿namespace NeuralNet
{
	/// <summary>
	/// Predefined activator types
	/// </summary>
	public enum ActivatorType
	{
		/// <summary>
		/// Logistic sigmoid activator
		/// </summary>
		SigmoidActivator,

		/// <summary>
		/// Hyperbolic Tangent activator
		/// </summary>
		TanhActivator,

		/// <summary>
		/// Rectified Linear Unit activator
		/// </summary>
		ReluActivator
	}
}
