﻿using System;
using System.Collections.Generic;
using System.Text;


namespace NeuralNet.Activators
{
	/// <summary>
	/// 
	/// </summary>
	public class ActivatorFactory
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static IActivator Produce(ActivatorType type)
		{
			switch (type)
			{
				case ActivatorType.SigmoidActivator:
					return new SigmoidActivation();

				case ActivatorType.TanhActivator:
					return new TanhActivator();
				case ActivatorType.ReluActivator:
					return new ReluActivator();
				default:
					throw new Exception("Unavailable type of activator");
			}
		}
	}
}
