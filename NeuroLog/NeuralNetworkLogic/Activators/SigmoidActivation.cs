﻿using System;

namespace NeuralNet.Activators
{
	/// <summary>
	/// 
	/// </summary>
	public class SigmoidActivation : IActivator
	{

		public double CalculateValue(double input)
		{
			return 1 / (1 + Math.Exp(-input));
		}

		public double CalculateDerivative(double input)
		{
			return input * (1 - input);
		}

		/// <summary>
		/// 
		/// </summary>
		private static Random _rand = new Random();

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public double GetRandomWeight()
		{
			return (_rand.NextDouble() * 2) - 1;
		}

		public static IActivator ActivatorInstance { get; set; } = new SigmoidActivation();
	}
}
