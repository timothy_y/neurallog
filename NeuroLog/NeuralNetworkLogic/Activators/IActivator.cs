﻿namespace NeuralNet.Activators
{
	/// <summary>
	/// 
	/// </summary>
	public interface IActivator
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		double CalculateValue(double input);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		double CalculateDerivative(double input);

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		double GetRandomWeight();
	}
}
