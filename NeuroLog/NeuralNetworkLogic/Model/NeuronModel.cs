﻿using System.Collections.Generic;

namespace NeuralNet.Model
{
	internal class NeuronModel
	{
		/// <summary>
		/// 
		/// </summary>
		public string Activator { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int NeuronIndex { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public List<double> Weights { get; set; }
	}
}
