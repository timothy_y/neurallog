﻿using System.Collections.Generic;

namespace NeuralNet.Model
{
	/// <summary>
	/// 
	/// </summary>
	internal class NetworkModel
	{
		/// <summary>
		/// 
		/// </summary>
		public NetworkModel()
		{
			Layers = new List<LayerModel>();
		}

		/// <summary>
		/// 
		/// </summary>
		public List<LayerModel> Layers { get; set; }
	}
}
