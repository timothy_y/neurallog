﻿using System.Collections.Generic;

namespace NeuralNet.Model
{
	/// <summary>
	/// 
	/// </summary>
	internal class LayerModel
	{
		/// <summary>
		/// 
		/// </summary>
		public List<NeuronModel> Neurons { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int LayerIndex { get; set; }
	}
}
