﻿
namespace NeuralNet.Configuration
{ 
	/// <summary>
	/// 
	/// </summary>
	public class NetworkConfiguration
	{
		/// <summary>
		/// Layer Configuration
		/// </summary>
		public readonly LayerConfiguration LayerConfiguration;

		/// <summary>
		/// Learning Rate. Default value 0.5.
		/// </summary>
		public double LearningRate { get; set; } = 0.5;

		/// <summary>
		/// Momentum. Default value 0.
		/// </summary>
		public double Momentum { get; set; } = 0;

		/// <summary>
		/// Creates a NetworkConfiguration object based in provided LayerConfiguration
		/// </summary>
		/// <param name="layerConfiguration">Layer configuration</param>
		public NetworkConfiguration(LayerConfiguration layerConfiguration)
		{
			LayerConfiguration = layerConfiguration;
		}
	}
}
