﻿using NeuralNet.Activators;

namespace NeuralNet.Configuration
{
	/// <summary>
	/// 
	/// </summary>
	internal class LayerConfigurationItem
	{
		/// <summary>
		/// 
		/// </summary>
		public readonly LayerType LayerType;

		/// <summary>
		/// 
		/// </summary>
		public readonly int NumberOfNeurons;

		/// <summary>
		/// 
		/// </summary>
		public readonly IActivator Activator;

		/// <summary>
		/// 
		/// </summary>
		public readonly IWeightInitializer WeightInitializer;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="layerType"></param>
		/// <param name="numberOfNeurons"></param>
		/// <param name="activator"></param>
		/// <param name="weightInitializer"></param>
		public LayerConfigurationItem(LayerType layerType, int numberOfNeurons, IActivator activator, IWeightInitializer weightInitializer)
		{
			WeightInitializer = weightInitializer;
			Activator = activator;
			LayerType = layerType;
			NumberOfNeurons = numberOfNeurons;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="layerType"></param>
		/// <param name="numberOfNeurons"></param>
		public LayerConfigurationItem(LayerType layerType, int numberOfNeurons)
		{
			LayerType = layerType;
			NumberOfNeurons = numberOfNeurons;
		}
	}
}
