﻿namespace NeuralNet.Connections
{
	/// <summary>
	/// 
	/// </summary>
	class Connection
	{
		/// <summary>
		/// 
		/// </summary>
		public readonly Neuron From;

		/// <summary>
		/// 
		/// </summary>
		public readonly Neuron To;

		/// <summary>
		/// 
		/// </summary>
		public double Weight;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="weight"></param>
		public Connection(Neuron from, Neuron to)
		{
			from.AddForwardConnections(this);
			From = from;

			to.AddBackwardConnections(this);
			To = to;

			Weight = to.WeightInitializer.InitializeWeight();
		}

		public Connection()
		{

		}

		/// <summary>
		/// 
		/// </summary>
		public double WeightedValue => From.Value * Weight;

		/// <summary>
		/// 
		/// </summary>
		public double WeightedError { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		/// <param name="learnRate"></param>
		/// <param name="error"></param>
		public void PropagateBack(double learnRate, double error)
		{
			WeightedError = error * Weight; 

			var weightDelta = learnRate * error * From.Value;
			Weight += weightDelta;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="weight"></param>
		public void SetWeight(double weight)
		{
			Weight = weight;
		}
	}
}
