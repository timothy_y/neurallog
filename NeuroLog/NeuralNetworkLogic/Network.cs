﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media.Audio;
using NeuralNet.Activators;
using NeuralNet.Configuration;
using NeuralNet.Connections;
using NeuralNet.Model;
using NeuralNet.Neurons;
using NeuralNetTset;
using NeuroLog.NeuralNetworkLogic.Neurons;
using Newtonsoft.Json;

namespace NeuralNet
{
	/// <summary>
	/// 
	/// </summary>
	[Serializable]
	class Network
	{

		/// <summary>
		/// 
		/// </summary>
		public List<Layer> Layers { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public InputLayer InputLayer  => Layers.OfType<InputLayer>().Single();

		/// <summary>
		/// 
		/// </summary>
		public OutputLayer OutputLayer => Layers.OfType<OutputLayer>().Single();

		/// <summary>
		/// 
		/// </summary>
		public HiddenLayer[] HiddenLayers => Layers.OfType<HiddenLayer>().ToArray();

		/// <summary>
		/// 
		/// </summary>
		public NetworkConfiguration Config { get; }

		/// <summary>
		/// 
		/// </summary>
		public int Epoch = 1;

		#region Create, recover and save network

		/// <summary>
		/// 
		/// </summary>
		/// <param name="config"></param>
		public Network(NetworkConfiguration config)
		{
			Config = config;
			Layers = CreateLayers(config.LayerConfiguration);
			CreateConnections();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="model"></param>
		public Network(NetworkModel model)
		{
			var layerConfiguration = CreateLayerConfiguration(model);
			Config = new NetworkConfiguration(layerConfiguration);
			Layers = CreateLayers(layerConfiguration);
			CreateConnections();
			AssignWeightValues(model);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public static async Task<Network> RecoverNetwork(string key)
		{
			var model = JsonConvert.DeserializeObject<NetworkModel>(await TempData.Memory.ReadFileAsync<string>(key));
			return new Network(model);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public async Task SaveNetwork(string key)
		{
			var model = new NetworkModel();

			foreach (var layer in Layers)
			{
				model.Layers.Add(new LayerModel
				{
					LayerIndex = layer.LayerIndex,
					Neurons = layer.Neurons
						.Select(q => q.ToNeuronModel())
						.ToList()
				});
			}

			await TempData.Memory.SaveFileAsync(key, JsonConvert.SerializeObject(model));
		}

		#endregion

		#region Init network methods

		/// <summary>
		/// 
		/// </summary>
		/// <param name="config"></param>
		/// <returns></returns>
		private List<Layer> CreateLayers(LayerConfiguration config)
		{
			List<Layer> layers = new List<Layer>();

			for (int i = 0; i < config.Layers.Count; i++)
			{
				var cur = config.Layers[i];

				switch (config.Layers[i].LayerType)
				{
					case LayerType.Input:
						var inputNeurons = new List<InputNeuron>();

						for (int j = 1; j <= cur.NumberOfNeurons; j++)
						{
							inputNeurons.Add(new InputNeuron(j));
						}
						inputNeurons.Add(new BiasNeuron(cur.NumberOfNeurons + 1));
						layers.Add(new InputLayer(inputNeurons.ToArray()));
						continue;
					case LayerType.Hidden:
						var hiddenNeurons = new List<Neuron>();
						for (int j = 1; j <= cur.NumberOfNeurons; j++)
						{
							hiddenNeurons.Add(new HiddenNeuron(j, cur.Activator, cur.WeightInitializer, new NeuronParam{LearnRate = Config.LearningRate}));
						}
						hiddenNeurons.Add(new BiasNeuron(cur.NumberOfNeurons + 1));
						layers.Add(new HiddenLayer(hiddenNeurons.ToArray(), i + 1));
						continue;
					case LayerType.Output:
						var outputNeurons = new List<OutputNeuron>();
						for (int j = 1; j <= cur.NumberOfNeurons; j++)
						{
							outputNeurons.Add(new OutputNeuron(j, cur.Activator, cur.WeightInitializer, new NeuronParam { LearnRate = Config.LearningRate }));
						}
						layers.Add(new OutputLayer(outputNeurons.ToArray(), i + 1));
						continue;
				}
			}
			return layers;
		}

		/// <summary>
		/// 
		/// </summary>
		private void CreateConnections()
		{
			if (!HiddenLayers.Any())
			{
				ConnectAdjacentLayers(InputLayer, OutputLayer);
			}
			else
			{
				var lastHiddenIndex = HiddenLayers.Length - 1;
				ConnectAdjacentLayers(InputLayer, HiddenLayers[0]);
				for (int i = 0; i < lastHiddenIndex; i++)
				{
					ConnectAdjacentLayers(HiddenLayers[i], HiddenLayers[i + 1]);
				}

				ConnectAdjacentLayers(HiddenLayers[lastHiddenIndex], OutputLayer);
			}
		}
	
		/// <summary>
		/// 
		/// </summary>
		/// <param name="fromLayer"></param>
		/// <param name="toLayer"></param>
		void ConnectAdjacentLayers(Layer fromLayer, Layer toLayer)
		{
			foreach (var fromNeuron in fromLayer.Neurons)
			{
				foreach (var toNeuron in toLayer.Neurons)
				{
					new Connection(fromNeuron, toNeuron);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="model"></param>
		private void AssignWeightValues(NetworkModel model)
		{
			foreach (var layer in Layers)
			{
				foreach (var neuron in layer.Neurons)
				{
					var weights = model
						.Layers
						.First(q => q.LayerIndex == layer.LayerIndex)
						.Neurons
						.First(q => q.NeuronIndex == neuron.NeuronIndex)
						.Weights;

					for (int i = 0; i < neuron.ForwardConnections.Count; i++)
					{
						neuron.ForwardConnections[i].SetWeight(weights[i]);
					}
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		private LayerConfiguration CreateLayerConfiguration(NetworkModel model)
		{
			var layerConfiguration = new LayerConfiguration();

			for (int i = 0; i < model.Layers.Count; i++)
			{
				var layer = model.Layers[i];

				if (i == 0)
				{
					//Count - 1 to not set bias neuron to config
					layerConfiguration.AddInputLayer(layer.Neurons.Count - 1);
				}
				else if (i != model.Layers.Count - 1)
				{
					//Count - 1 to not set bias neuron to config
					var activator = (IActivator)Activator.CreateInstance(Type.GetType(layer.Neurons.First().Activator));
					layerConfiguration.AddHiddenLayer(layer.Neurons.Count - 1, activator);
				}
				else
				{
					var activator = (IActivator)Activator.CreateInstance(Type.GetType(layer.Neurons.First().Activator));
					layerConfiguration.AddOutputLayer(layer.Neurons.Count, activator);
				}
			}

			return layerConfiguration;
		}

		#endregion

		/// <summary>
		/// 
		/// </summary>
		/// <param name="input"></param>
		/// <returns></returns>
		public double[] FeedForward(double[] input)
		{
			InputLayer.SetInputValues(input);
			foreach (var hiddenLayer in HiddenLayers)
			{
				hiddenLayer.FeedForward();
			}
				
			OutputLayer.FeedForward();
			return OutputLayer.Values;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="target"></param>
		public void PropagateBack(double[] target)
		{
			OutputLayer.SetTargetValues(target);
			OutputLayer.PropagateBack();

			foreach (var hiddenLayer in HiddenLayers.Reverse())
			{
				hiddenLayer.PropagateBack();
			}
		}

		/// <summary>
		/// Process an input data on a previously trained model
		/// </summary>
		/// <param name="input">Input parametres</param>
		/// <param name="target">Target output</param>
		/// <returns>Error</returns>
		public List<double> Guess(List<double> input)
		{
			
			FeedForward(input.ToArray());

			Epoch++;

			return FeedForward(input.ToArray()).ToList();
		}
	}
}
