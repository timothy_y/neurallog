﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NeuralNet.WeightInitializers
{
	/// <summary>
	/// 
	/// </summary>
	public class DefaultWeightInitializer : IWeightInitializer
	{
		/// <summary>
		/// 
		/// </summary>
		private readonly Random _rnd;

		/// <summary>
		/// 
		/// </summary>
		public DefaultWeightInitializer()
		{
			_rnd = new Random();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="numberOfInputs"></param>
		/// <param name="numberOfOutputs"></param>
		/// <returns></returns>
		public double InitializeWeight(int numberOfInputs, int numberOfOutputs)
		{
			double magnitutde = 1 / Math.Sqrt(numberOfInputs);
			return _rnd.NextDouble() * magnitutde - magnitutde / 2;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="numberOfInputs"></param>
		/// <param name="numberOfOutputs"></param>
		/// <returns></returns>
		public double InitializeWeight()
		{
			return (_rnd.NextDouble() * 2) - 1;
		}
	}
}
