﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using NeuralNetTset;
using NeuroLog;
using System.Threading;
using Windows.UI.Xaml.Controls;
using Microsoft.Toolkit.Uwp.Helpers;
using Newtonsoft.Json;

namespace NeuralNet
{
	/// <summary>
	/// 
	/// </summary>
	/// <param name="target"></param>
	/// <param name="output"></param>
	/// <returns></returns>
	delegate bool CheckCorrect(double[] target, double[] output);

	/// <summary>
	/// 
	/// </summary>
	class Trainer
	{
		/// <summary>
		/// 
		/// </summary>
		public Network Network { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public Sample[] Cases { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int CaseCount { get; set; }

		/// <summary>
		/// 
		/// </summary>
		private readonly CheckCorrect _checkCorrect;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="net"></param>
		/// <param name="cases"></param>
		/// <param name="checkCorrect"></param>
		/// <param name="testCases"></param>
		public Trainer(Network net, Sample[] cases, CheckCorrect checkCorrect)
		{
			Network = net;
			Cases = cases;
			CaseCount = cases.Length;

			if (CaseCount == 0)
			{
				throw new Exception("Don't have any samples to train with!");
			}

			var sampleSample = cases[0];

			if (net.Config.LayerConfiguration.Layers[0].NumberOfNeurons != sampleSample.input.Length)
			{
				throw new Exception(string.Format(
					"Net input size: {0}, doesn't match sample input size: {1}.",
					net.InputLayer.Neurons.Length, sampleSample.input.Length));
			}

			if (net.OutputLayer.Neurons.Length != sampleSample.target.Length)
			{
				throw new Exception(string.Format(
					"Net output size: {0}, doesn't match sample target size: {1}.",
					net.OutputLayer.Neurons.Length, sampleSample.target.Length));
			}

			_checkCorrect = checkCorrect;
		}

		/// <summary>
		/// 
		/// </summary>
		public long EpochSize => CaseCount;
		
		/// <summary>
		/// 
		/// </summary>
		public long EpochCount { get; private set; }
		
		/// <summary>
		/// 
		/// </summary>
		public long CurrentSampleCount { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public long TotalSampleCount { get; private set; }

		/// <summary>
		/// 
		/// </summary>
		public void TrainAllCases()
		{
			foreach (var sample in Cases)
			{
				Network.FeedForward(sample.input);
				Network.PropagateBack(sample.target);
				CurrentSampleCount++;
				TotalSampleCount++;
			}
			EpochCount++;
			CurrentSampleCount = 0;
		}

		/// <summary>
		/// 
		/// </summary>
		public async void StartTraining()
		{
			ConsoleLog.WriteLine($"Init training with {CaseCount} training cases. Starting first epoch.");
			ConsoleLog.WriteLine("================================");
			await Task.Run(TrainUntilDone);
			PlotViewer.UpdatePlot();
			ConsoleLog.WriteLine("Learning finished");

			var saveDialog = new ContentDialog
			{
				Title = "Save network",
				Content = "Do you wont to save network to the cloud?",
				CloseButtonText = "No, thanks",
				PrimaryButtonText = "Yes, please",
				DefaultButton = ContentDialogButton.Primary
			};

			var key = "mnist_test";

			saveDialog.PrimaryButtonClick += async (sender, args) =>
			{
				await Network.SaveNetwork(key);
				ConsoleLog.WriteLine($"Network saved with key: '{key}'");

			};

			TempData.LastNetwork = Network;
			var result = await saveDialog.ShowAsync();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public async Task TrainUntilDone()
		{
			var start = DateTime.UtcNow;
			var trainingAccuracy = MeasureAccuracy(Cases);
			int previosAccuracy = 0; 
			while (trainingAccuracy.Item1 != trainingAccuracy.Item2)
			{
				var epochStart = DateTime.UtcNow;
				ShuffleCases();
				var trainingStart = DateTime.UtcNow;
				TrainAllCases();
				var trainingEnd = DateTime.UtcNow;
				trainingAccuracy = MeasureAccuracy(Cases);

				var newAccuracy = Convert.ToInt32(GetAccuracy(trainingAccuracy));
				if (previosAccuracy != newAccuracy)
				{
					PlotViewer.AddValue(new Point(EpochCount, newAccuracy));

					await TempData.CurrentPage.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High,
					() =>
					{
						ConsoleLog.WriteLine(String.Format($"Epoch {EpochCount} complete:"));
						ConsoleLog.WriteLine($"Correct {GetAccuracy(trainingAccuracy)}% - {trainingAccuracy.Item1} from {trainingAccuracy.Item2} cases.");
						ConsoleLog.WriteLine($"Training time: {trainingEnd - trainingStart:mm\\:ss}");
						ConsoleLog.WriteLine($"Epoch time: {DateTime.UtcNow - epochStart:mm\\:ss}");
						ConsoleLog.WriteLine($"Run time: {DateTime.UtcNow - start:mm\\:ss}");
						ConsoleLog.WriteLine("================================");
						PlotViewer.UpdatePlot();
					});
				}
				previosAccuracy = Convert.ToInt32(GetPercent(trainingAccuracy.Item1, trainingAccuracy.Item2));

				if (TempData.IsStop)
				{
					TempData.IsStop = false;
					break;
				}
			}
			
		}

		private static readonly Random _rand = new Random();

		public void ShuffleCases()
		{
			for (int i = 0; i < CaseCount; i++)
			{
				int r = (int) (_rand.NextDouble() * CaseCount);
				var rTemp = Cases[r];
				Cases[r] = Cases[i];
				Cases[i] = rTemp;
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		private double GetAccuracy(Tuple<int, int> item) => GetPercent(item.Item1, item.Item2);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="d"></param>
		/// <param name="n"></param>
		/// <returns></returns>
		private double GetPercent(int d, int n) => 100 * (float)d / n;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="samples"></param>
		/// <returns></returns>
		private Tuple<int, int> MeasureAccuracy(Sample[] samples)
		{
			var correctCount = (
					from sample in samples
					let output = Network.FeedForward(sample.input)
					where _checkCorrect(sample.target, output)
					select 1)
				.Count();
			return Tuple.Create(correctCount, samples.Length);
		}
	}
}
