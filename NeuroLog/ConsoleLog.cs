﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace NeuralNetTset
{
	/// <summary>
	/// 
	/// </summary>
	public static class ConsoleLog
	{
		/// <summary>
		/// 
		/// </summary>
		public static void WriteLine(object text)
		{
			TempData.Console.Text += String.Concat(Convert.ToString(text), Environment.NewLine);
			ScrollConsole();
		}

		/// <summary>
		/// 
		/// </summary>
		public static void Write(object text)
		{
			TempData.Console.Text += Convert.ToString(text);
		}

		/// <summary>
		/// 
		/// </summary>
		public static void Clear()
		{
			TempData.Console.Text = String.Empty; 
		}

		/// <summary>
		/// 
		/// </summary>
		private static void ScrollConsole()
		{
			var grid = (Grid)VisualTreeHelper.GetChild(TempData.Console, 0);
			for (var i = 0; i <= VisualTreeHelper.GetChildrenCount(grid) - 1; i++)
			{
				object obj = VisualTreeHelper.GetChild(grid, i);
				if (!(obj is ScrollViewer)) continue;
				((ScrollViewer)obj).ChangeView(0.0f, ((ScrollViewer)obj).ExtentHeight, 1.0f);
				break;
			}
		}
	}
}

